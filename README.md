Alioth-GitLab migration scripts
===============================

This repository contains tools to facilitate the Alioth to GitLab
transition.

More documentation about the migration in the [AliothMigratioh page in
the Debian wiki](https://wiki.debian.org/Salsa/AliothMigration).

disable-repository
------------------

Simple script written by Raphael Hertzog and heavily modified by
Antoine Beaupré to disable a repository on Alioth. It adds a hook that
denies pushes and a warning in the description.

migrate-repo
------------

Does the same, but attempts to import the repository into GitLab
first. Basically a rewrite of [Christopher Berg's shell
script](http://www.df7cb.de/blog/2017/Salsa_batch_import.html). It's faster because it works around that [pesky GitLab
bug](https://gitlab.com/gitlab-org/gitlab-ce/issues/42415) and it will wait until the repo is actually live before
disabling the repo on Alioth.

missing bits
------------

 * modifying the `debian/control` file in relevant packages should
   still be done by hand, although it's technically possible to create
   a commit that would do that on alioth first

 * the [AliothRewriter](https://salsa.debian.org/salsa/AliothRewriter) step needs to be done by hand as well
